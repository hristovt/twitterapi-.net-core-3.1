﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RestSharp;
using Tweetinvi;
using Tweetinvi.Auth;
using Tweetinvi.Exceptions;
using Tweetinvi.Models;
using TwitterAPI.Models.Twitter;

namespace TwitterAPI.Services.TwitterAPI
{
    public class PostTweetsAPI : IPostTweetsAPI
    {
        private readonly IAuthenticationRequestStore _tweeterAuthRequestStore = new LocalAuthenticationRequestStore();

        private readonly IWebHostEnvironment _environment;
        public TwitterApiOptions Options { get; }
        private string AuthorizationKey { get; set; }
        private string AuthorizationSecret { get; set; }


        public PostTweetsAPI(
            IOptions<TwitterApiOptions> optionsAccessor,
            IWebHostEnvironment environment)
        {
            Options = optionsAccessor.Value;
            _environment = environment;
        }

        public string GetTwitterApiAuthentication(Tweetinvi.Models.IAuthenticationRequest authenticationRequestToken, string authenticationRequestId)
        {
            //var authenticationRequestToken = await appClient.Auth.RequestAuthenticationUrlAsync(redirectURL);
            _tweeterAuthRequestStore.AddAuthenticationTokenAsync(authenticationRequestId, authenticationRequestToken);
            AuthorizationKey = authenticationRequestToken.AuthorizationKey;
            AuthorizationSecret = authenticationRequestToken.AuthorizationSecret;

            return authenticationRequestToken.AuthorizationURL;
        }

        //Step 3: POST oauth/access_token
        public async Task<UserOauthTokensModel> GetUserAuthorize(string oAuthToken, string oAuthVerifier)
        {
            UserOauthTokensModel respObj = new UserOauthTokensModel();
            try
            {
                string requestUrl = "https://api.twitter.com/oauth/access_token";

                var client = new RestClient(requestUrl);
                var request = new RestRequest(Method.POST);
                request.AddQueryParameter("oauth_token", oAuthToken);
                request.AddQueryParameter("oauth_verifier", oAuthVerifier);
                var response = client.Execute(request, Method.POST);
                if (response.IsSuccessful)
                {
                    var tokens = HttpUtility.ParseQueryString(response.Content);
                    string json = JsonConvert.SerializeObject(tokens.Cast<string>().ToDictionary(k => k, v => tokens[v]));
                    respObj = JsonConvert.DeserializeObject<UserOauthTokensModel>(json);

                    var userClient = new TwitterClient(Options.TwitterApiKey, Options.TwitterApiSecretKey, respObj.UserAccessToken, respObj.UserAccessTokenSecret);
                    var authenticatedUser = await userClient.Users.GetAuthenticatedUserAsync();
                    respObj.UserName = authenticatedUser.Name;
                    respObj.UserScreenName = authenticatedUser.ScreenName;
                    respObj.UserImageUrl = authenticatedUser.ProfileImageUrlFullSize;
                    respObj.RequestAuthToken = oAuthToken;
                    respObj.RequestAuthToken = oAuthVerifier;
                    respObj.IsSuccessful = true;
                    WriteTokensFile(respObj);
                }
                else
                {
                    respObj.Message = "Unauthorized - Twitter API  Authentication credentials were missing or incorrect";
                    respObj.IsSuccessful = false;
                }
            }
            catch (TwitterException)
            {
                respObj.Message = "Twitter Exception - Twitter API  Authentication credentials were missing or incorrect";
                respObj.IsSuccessful = false;
            }
            catch (Exception)
            {
                // Other system exceptions like SocketException if you do not have internet
                respObj.Message = "System Exception";
                respObj.IsSuccessful = false;
            }
            return respObj;
        }

        public async Task<Tweetinvi.Models.ITweet> PostTweetAsync(UserOauthTokensModel respObj)
        {
            Tweetinvi.Models.ITweet tweet = null;
            try
            {
                var userClient = new TwitterClient(Options.TwitterApiKey, Options.TwitterApiSecretKey, respObj.UserAccessToken, respObj.UserAccessTokenSecret);
                var authenticatedUser = await userClient.Users.GetAuthenticatedUserAsync();
                var userImage = authenticatedUser.ProfileImageUrlFullSize;
                var userName = authenticatedUser.Name;
                var userScreenName = authenticatedUser.ScreenName;

                tweet = await userClient.Tweets.PublishTweetAsync(string.Concat("Hello Twitter - Hristo Hristov test - ", DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss")));
                //tweet = await userClient.Tweets.PublishTweetAsync("https://rtmp.attn.live/playlist2.m3u8");
            }
            catch (TwitterException)
            {
                respObj.Message = "Twitter exception - Bad Authentication";
            }
            catch (Exception)
            {
                respObj.Message = "System Exception - Bad Authentication";
            }
            return tweet;
        }

        public void WriteTokensFile(UserOauthTokensModel respObj)
        {
            string json = JsonConvert.SerializeObject(respObj);
            string fullFilePath = string.Format(_environment.ContentRootPath + "\\Tokens\\Twitter\\");
            try
            {
                File.WriteAllText(fullFilePath + "TwitterTokens-" + "userId" + ".json", json);
            }
            catch (Exception)
            {
                //errorString = e.Message;
                //result = false;
            }
        }

        public UserOauthTokensModel ReadTokensFile()
        {
            UserOauthTokensModel respObj = new UserOauthTokensModel();
            try
            {
                string fullFilePath = string.Format(_environment.ContentRootPath + "\\Tokens\\Twitter\\");
                string json = File.ReadAllText(fullFilePath + "TwitterTokens-" + "userId" + ".json");
                respObj = JsonConvert.DeserializeObject<UserOauthTokensModel>(json);
                respObj.IsSuccessful = true;
                return respObj;
            }
            catch (Exception)
            {
                respObj.IsSuccessful = false;
            }
            return respObj;
        }

        public async Task<bool> CheckUserAuthentication(UserOauthTokensModel respObj)
        {
            bool isAuthenticated = false;
            try
            {
                var userClient = new TwitterClient(Options.TwitterApiKey, Options.TwitterApiSecretKey, respObj.UserAccessToken, respObj.UserAccessTokenSecret);
                var authenticatedUser = await userClient.Users.GetAuthenticatedUserAsync();
                isAuthenticated = true;
            }
            catch (TwitterException)
            {
                respObj.Message = "Unauthorized - Authentication credentials were missing or incorrect.";
            }
            catch (Exception)
            {
                respObj.Message = "System Exception - Bad Authentication";
            }
            return isAuthenticated;
        }
    }
}
