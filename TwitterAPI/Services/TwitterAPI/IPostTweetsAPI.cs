﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RestSharp;
using Tweetinvi;
using Tweetinvi.Auth;
using Tweetinvi.Exceptions;
using Tweetinvi.Models;
using TwitterAPI.Models.Twitter;


namespace TwitterAPI.Services.TwitterAPI
{
    public interface IPostTweetsAPI
    {
        public string GetTwitterApiAuthentication(Tweetinvi.Models.IAuthenticationRequest authenticationRequestToken, string authenticationRequestId);
        public Task<Tweetinvi.Models.ITweet> PostTweetAsync(UserOauthTokensModel respObj);
        public Task<UserOauthTokensModel> GetUserAuthorize(string oAuthToken, string oAuthVerifier);
        public void WriteTokensFile(UserOauthTokensModel respObj);
        public UserOauthTokensModel ReadTokensFile();
        public Task<bool> CheckUserAuthentication(UserOauthTokensModel respObj);
    }
}
