﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TwitterAPI.Services.TwitterAPI
{
    public class TwitterApiOptions
    {
        public string TwitterApiKey { get; set; }
        public string TwitterApiSecretKey { get; set; }
        public string TwitterBearerToken { get; set; }
        public string TwitterAccessToken { get; set; }
        public string TwitterAccessTokenSecret { get; set; }
    }
}
