﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TwitterAPI.Models.Twitter
{
    public class UserOauthTokensModel
    {
        //Temporary ApiClient credentials:
        public string RequestAuthToken { get; set; }
        public string RequestAuthVerifier { get; set; }

        //Token credentials:
        [JsonProperty("oauth_token")]
        public string UserAccessToken { get; set; }

        [JsonProperty("oauth_token_secret")]
        public string UserAccessTokenSecret { get; set; }

        [JsonProperty("user_id")]
        public string TwetterUserId { get; set; }

        //Additional properties
        public string Message { get; set; }
        public bool IsSuccessful { get; set; }
        public string UserName { get; set; }
        public string UserScreenName { get; set; }
        public string UserImageUrl { get; set; }
    }
}
