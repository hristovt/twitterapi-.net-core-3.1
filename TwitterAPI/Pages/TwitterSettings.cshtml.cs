using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RestSharp;
using Tweetinvi;
using Tweetinvi.Auth;
using Tweetinvi.Exceptions;
using Tweetinvi.Parameters;
using TwitterAPI.Models.Twitter;
using TwitterAPI.Services.TwitterAPI;

namespace TwitterAPI.Pages
{
    public class TwitterSettings : PageModel
    {
        private static readonly IAuthenticationRequestStore _tweeterAuthRequestStore = new LocalAuthenticationRequestStore();
        public TwitterApiOptions Options { get; }

        private readonly IPostTweetsAPI _twitterApi;

        public TwitterSettings(IOptions<TwitterApiOptions> optionsAccessor, IPostTweetsAPI twitterApi)
        {
            Options = optionsAccessor.Value;
            _twitterApi = twitterApi;
        }
        [TempData]
        public string StatusMessage { get; set; }
        private string OAuthToken { get; set; }

        private string OAuthVerifier { get; set; }
        private string AuthorizationKey { get; set; }
        private string AuthorizationSecret { get; set; }
        private string QueryString { get; set; }
        public UserOauthTokensModel TwitterUser { get; set; }

        public async Task<IActionResult> OnGet()
        {
            OAuthToken = HttpContext.Request.Query["oauth_token"].ToString();
            OAuthVerifier = HttpContext.Request.Query["oauth_verifier"].ToString();
            //QueryString = Request.QueryString.Value;

            this.StatusMessage = string.Empty;
            await LoadAsync();
            return Page();
        }

        private async Task LoadAsync()
        {
            bool isUserAuthorize = false;
            bool isAuthenticatedUser = false;
            if (!string.IsNullOrWhiteSpace(OAuthToken) && !string.IsNullOrWhiteSpace(OAuthVerifier))
            {
                TwitterUser = await _twitterApi.GetUserAuthorize(this.OAuthToken, this.OAuthVerifier);
                isUserAuthorize = TwitterUser.IsSuccessful ? true : false;
                isAuthenticatedUser = TwitterUser.IsSuccessful ? true : false;
            }
            if (!isUserAuthorize)
            {
                UserOauthTokensModel storedTwitterUser = _twitterApi.ReadTokensFile();
                isAuthenticatedUser = await _twitterApi.CheckUserAuthentication(storedTwitterUser);
                if (isAuthenticatedUser)
                {
                    TwitterUser = storedTwitterUser;
                }
            }
            this.StatusMessage = isAuthenticatedUser ? "Your Twitter account is authenticated." : "Please, authenticate your Twitter account!";
        }

        //Step 1: POST oauth/access_token and Step 2: GET oauth/authorize
        //documentation-https://developer.twitter.com/en/docs/authentication/oauth-1-0a/obtaining-user-access-tokens
        public async Task<ActionResult> OnPostTwitterSigninAsync()
        {
            try
            {
                TwitterClient appClient = new TwitterClient(Options.TwitterApiKey, Options.TwitterApiSecretKey);
                string authenticationRequestId = Guid.NewGuid().ToString();
                string redirectPath = Request.Scheme + "://" + Request.Host.Value + "/Identity/Account/Manage/TwitterSettings";
                string redirectURL = _tweeterAuthRequestStore.AppendAuthenticationRequestIdToCallbackUrl(redirectPath, authenticationRequestId);
                Tweetinvi.Models.IAuthenticationRequest authenticationRequestToken = await appClient.Auth.RequestAuthenticationUrlAsync(redirectURL);
                return new RedirectResult(_twitterApi.GetTwitterApiAuthentication(authenticationRequestToken, authenticationRequestId));
            }
            catch (TwitterException)
            {
                this.StatusMessage = "Unauthorized - Twitter API  Authentication credentials were missing or incorrect";
            }
            catch (Exception)
            {
                // Other system exceptions like SocketException if you do not have internet
                this.StatusMessage = "System Exception";
            }


            return Page();
        }

        public async Task<ActionResult> OnPostTweetUpdateAsync()
        {
            UserOauthTokensModel respObj = _twitterApi.ReadTokensFile();

            //UserOauthTokensModel respObj = await _twitterApi.GetUserAuthorize(Request.Form["OAuthToken"], Request.Form["OAuthVerifier"]);
            if (respObj.IsSuccessful)
            {
                //string json = JsonConvert.SerializeObject(respObj);

                //_twitterApi.WriteTokensFile(respObj);
                var tweet = await _twitterApi.PostTweetAsync(respObj);
                this.StatusMessage = "Success";
            }
            else
            {
                this.StatusMessage = respObj.Message;
            }
            return Page();
        }
    }
}
